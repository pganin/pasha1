/*
class variables {


  object global {
    val cookies = Map(
      "cookie" -> "ms.test.sid=s%3A5EiMH6JR0a5lmMGwvDy7MNC4J6Nhxssl.bCIHabvhmkyIX3uGWw9e7YzS8dht3x%2F6YngsguYYaFU;",
      "content-type" -> "application/json"
    )
    val baseURL = "https://test.ceicdata.com"
  }

  object createInsight {
    var req = "/api/insight?ct=1517219161792"
    var body = "createInsight.txt"
  }

  object copyInsight {
    var req = "/api/insight/6c9a516e-f0a7-443a-85e8-dc58b24bb39f/copy?name=123_copy&_=1511182232255"
  }

  object applyFilter {
    var req = "/api/series"
    var body = "applyFilter.txt"
  }

  object openInsight {
    var req = "/api/insight/a83a7b0e-cc23-4f2e-9cc9-184008d856e9/series?_=1511524535924"
  }

  object expandTree {
    var req1 = "/api/nodes/main_databases?filter=%7B%22status%22%3A%5B%5D%2C%22name_only%22%3Afalse%2C%22subscribed_only%22%3Afalse%2C%22database_series_sort_type%22%3A%7B%22dir%22%3A%22asc%22%2C%22field%22%3A%22TREE%22%7D%2C%22observation_filter%22%3A%7B%22first_obs_before_selected%22%3Afalse%2C%22last_obs_after_selected%22%3Afalse%2C%22first_obs_before%22%3Anull%2C%22last_obs_after%22%3Anull%7D%2C%22key_series_only%22%3Afalse%2C%22new_series_only%22%3Afalse%2C%22unit%22%3A%5B%5D%2C%22source%22%3A%5B%5D%2C%22region%22%3A%5B%5D%2C%22databases%22%3A%5B%5D%2C%22countries%22%3A%5B%5D%2C%22frequencies%22%3A%5B%5D%2C%22indicator%22%3A%5B%5D%2C%22tree_matches_mode%22%3A%22MISMATCHES_ONLY%22%2C%22load_full_tree%22%3Atrue%7D&search_tab=tree&_=1511519704885"
    var req2 = "/api/nodes/databases/GLOBAL/topics?filter=%7B%22status%22%3A%5B%5D%2C%22name_only%22%3Afalse%2C%22subscribed_only%22%3Afalse%2C%22database_series_sort_type%22%3A%7B%22dir%22%3A%22asc%22%2C%22field%22%3A%22TREE%22%7D%2C%22observation_filter%22%3A%7B%22first_obs_before_selected%22%3Afalse%2C%22last_obs_after_selected%22%3Afalse%2C%22first_obs_before%22%3Anull%2C%22last_obs_after%22%3Anull%7D%2C%22key_series_only%22%3Afalse%2C%22new_series_only%22%3Afalse%2C%22unit%22%3A%5B%5D%2C%22source%22%3A%5B%5D%2C%22region%22%3A%5B%5D%2C%22databases%22%3A%5B%5D%2C%22countries%22%3A%5B%5D%2C%22frequencies%22%3A%5B%5D%2C%22indicator%22%3A%5B%5D%2C%22tree_matches_mode%22%3A%22MISMATCHES_ONLY%22%2C%22load_full_tree%22%3Atrue%7D&search_tab=tree&_=1511519704886"
    var req3 = "/api/nodes/topics/TP111/sections?filter=%7B%22status%22%3A%5B%5D%2C%22name_only%22%3Afalse%2C%22subscribed_only%22%3Afalse%2C%22database_series_sort_type%22%3A%7B%22dir%22%3A%22asc%22%2C%22field%22%3A%22TREE%22%7D%2C%22observation_filter%22%3A%7B%22first_obs_before_selected%22%3Afalse%2C%22last_obs_after_selected%22%3Afalse%2C%22first_obs_before%22%3Anull%2C%22last_obs_after%22%3Anull%7D%2C%22key_series_only%22%3Afalse%2C%22new_series_only%22%3Afalse%2C%22unit%22%3A%5B%5D%2C%22source%22%3A%5B%5D%2C%22region%22%3A%5B%5D%2C%22databases%22%3A%5B%5D%2C%22countries%22%3A%5B%5D%2C%22frequencies%22%3A%5B%5D%2C%22indicator%22%3A%5B%5D%2C%22tree_matches_mode%22%3A%22MISMATCHES_ONLY%22%2C%22load_full_tree%22%3Atrue%7D&search_tab=tree&_=1511519704887"
    var req4 = "/api/nodes/sections/SC345/tables?filter=%7B%22status%22%3A%5B%5D%2C%22name_only%22%3Afalse%2C%22subscribed_only%22%3Afalse%2C%22database_series_sort_type%22%3A%7B%22dir%22%3A%22asc%22%2C%22field%22%3A%22TREE%22%7D%2C%22observation_filter%22%3A%7B%22first_obs_before_selected%22%3Afalse%2C%22last_obs_after_selected%22%3Afalse%2C%22first_obs_before%22%3Anull%2C%22last_obs_after%22%3Anull%7D%2C%22key_series_only%22%3Afalse%2C%22new_series_only%22%3Afalse%2C%22unit%22%3A%5B%5D%2C%22source%22%3A%5B%5D%2C%22region%22%3A%5B%5D%2C%22databases%22%3A%5B%5D%2C%22countries%22%3A%5B%5D%2C%22frequencies%22%3A%5B%5D%2C%22indicator%22%3A%5B%5D%2C%22tree_matches_mode%22%3A%22MISMATCHES_ONLY%22%2C%22load_full_tree%22%3Atrue%7D&search_tab=tree&_=1511519704888"
    var req5 = "/api/nodes/tables/TB1541/series?filter=%7B%22status%22%3A%5B%5D%2C%22name_only%22%3Afalse%2C%22subscribed_only%22%3Afalse%2C%22database_series_sort_type%22%3A%7B%22dir%22%3A%22asc%22%2C%22field%22%3A%22TREE%22%7D%2C%22observation_filter%22%3A%7B%22first_obs_before_selected%22%3Afalse%2C%22last_obs_after_selected%22%3Afalse%2C%22first_obs_before%22%3Anull%2C%22last_obs_after%22%3Anull%7D%2C%22key_series_only%22%3Afalse%2C%22new_series_only%22%3Afalse%2C%22unit%22%3A%5B%5D%2C%22source%22%3A%5B%5D%2C%22region%22%3A%5B%5D%2C%22databases%22%3A%5B%5D%2C%22countries%22%3A%5B%5D%2C%22frequencies%22%3A%5B%5D%2C%22indicator%22%3A%5B%5D%2C%22tree_matches_mode%22%3A%22MISMATCHES_ONLY%22%2C%22load_full_tree%22%3Atrue%7D&search_tab=tree&_=1511519704889"
  }

  object searchKeyword {
    var req = "/api/series"
    var body = "searchKeyword.txt"
  }

  object openDatasets {
    var req = "/api/data_sets?parameters=%7B%22main_filter%22%3A%7B%22tree_matches_mode%22%3A%22MISMATCHES_ONLY%22%2C%22indicator%22%3A%5B%5D%2C%22frequencies%22%3A%5B%5D%2C%22countries%22%3A%5B%5D%2C%22region%22%3A%5B%5D%2C%22source%22%3A%5B%5D%2C%22unit%22%3A%5B%5D%2C%22new_series_only%22%3Afalse%2C%22key_series_only%22%3Afalse%2C%22observation_filter%22%3A%7B%22last_obs_after%22%3Anull%2C%22first_obs_before%22%3Anull%2C%22last_obs_after_selected%22%3Afalse%2C%22first_obs_before_selected%22%3Afalse%7D%2C%22database_series_sort_type%22%3A%7B%22field%22%3A%22TREE%22%2C%22dir%22%3A%22asc%22%7D%2C%22subscribed_only%22%3Afalse%2C%22name_only%22%3Afalse%2C%22status%22%3A%5B%5D%2C%22databases%22%3A%5B%5D%2C%22database_series_filter%22%3A%22%22%7D%2C%22can_use_cache%22%3Atrue%2C%22from%22%3A0%2C%22to%22%3A50%7D&search_tab=DATASETS&_=1517220044195"
  }

  object copyGalleryInsight {
    var req = "/api/insight/85fd21df-a311-40f6-8f5c-b570d5e92971/copy?name=gallery+insight_copy&_=1517219234205"
  }

  object openAllTab {
    var req = "/api/data_sets?parameters=%7B%22additional_filter%22%3A%7B%22sort_by%22%3A%22NEW%22%2C%22sort_order%22%3A%22desc%22%7D%2C%22can_use_cache%22%3Atrue%2C%22from%22%3A0%2C%22to%22%3A10%7D&search_tab=ALL&_=1517220044193"
  }

  object openReleasesTab {
    var req = "/api/release_schedule/indicators?parameters=%7B%22main_filter%22%3A%7B%22tree_matches_mode%22%3A%22MISMATCHES_ONLY%22%2C%22indicator%22%3A%5B%5D%2C%22frequencies%22%3A%5B%5D%2C%22countries%22%3A%5B%5D%2C%22region%22%3A%5B%5D%2C%22source%22%3A%5B%5D%2C%22unit%22%3A%5B%5D%2C%22new_series_only%22%3Afalse%2C%22key_series_only%22%3Afalse%2C%22observation_filter%22%3A%7B%22last_obs_after%22%3Anull%2C%22first_obs_before%22%3Anull%2C%22last_obs_after_selected%22%3Afalse%2C%22first_obs_before_selected%22%3Afalse%7D%2C%22database_series_sort_type%22%3A%7B%22field%22%3A%22TREE%22%2C%22dir%22%3A%22asc%22%7D%2C%22subscribed_only%22%3Afalse%2C%22name_only%22%3Afalse%2C%22status%22%3A%5B%5D%2C%22databases%22%3A%5B%5D%2C%22database_series_filter%22%3A%22%22%7D%2C%22additional_filter%22%3A%7B%22status%22%3A%5B%5D%2C%22from%22%3A%222018-01-28T21%3A00%3A00.000Z%22%2C%22to%22%3A%222018-01-29T20%3A59%3A59.999Z%22%7D%2C%22from%22%3A0%2C%22to%22%3A50%7D&search_tab=release_schedule&_=1517220044194"
  }

  object download100Series {
    var req = "/api/v1/export/series/xlsx"
    var body = "download100Series.txt"
  }

  object addChart {
    var req = "/api/insight/54de69b9-b7e0-4493-9d8f-2d221a51b9cf/views/119fdf7f-af1c-4e2c-85d8-c17f7b3fee93/elements?ct=1517218094082"
    var body = "addChart.txt"
  }

  object addTable {
    var req = "/api/insight/f5ded8cf-7507-4044-a47c-a6e7c5bd21a7/views/3d0d023f-7b09-4012-9336-3b78a86d5a19/elements?ct=1517218958292"
    var body = "addTable.txt"
  }

  object addMap {
    var req = "/api/insight/39a075ab-021d-4a9c-a219-0c03a9658c0f/views/f469f057-1dea-419a-8c09-d180b312e320/elements?ct=1517218784505"
    var body = "addMap.txt"
  }

  object sortingLanding {
    var req = "/api/search/landing/insights?q=%7B%22count%22%3A35%2C%22group%22%3A%22all%22%2C%22sub_group%22%3A%22gallery%22%2C%22from%22%3A0%2C%22text%22%3A%22%22%2C%22tags%22%3Anull%2C%22categories%22%3A%5B%5D%2C%22gallery_categories%22%3A%5B%5D%2C%22focus_economics_category%22%3A%5B%5D%2C%22parents%22%3A%5B%5D%2C%22sort%22%3A%7B%22name%22%3A-1%7D%2C%22search_by%22%3A%22content%22%2C%22search_content_attributes%22%3A%5B%22insights_attributes%22%2C%22insights_content%22%2C%22series_attributes%22%5D%7D&_=1517220044221"
  }

  object searchLanding {
    var req1 = "/api/search/landing/insights?q=%7B%22count%22%3A35%2C%22group%22%3A%22my%22%2C%22sub_group%22%3A%22gallery%22%2C%22from%22%3A0%2C%22text%22%3A%22map%22%2C%22tags%22%3Anull%2C%22categories%22%3A%5B%5D%2C%22gallery_categories%22%3A%5B%5D%2C%22focus_economics_category%22%3A%5B%5D%2C%22parents%22%3A%5B%5D%2C%22sort%22%3A%7B%22last_edit_date%22%3A-1%7D%2C%22search_by%22%3A%22content%22%2C%22search_content_attributes%22%3A%5B%22insights_attributes%22%2C%22insights_content%22%2C%22series_attributes%22%5D%7D&_=1517220044208"
    var req2 = "/api/search/landing/series?q=%7B%22count%22%3A35%2C%22group%22%3A%22my%22%2C%22sub_group%22%3A%22gallery%22%2C%22from%22%3A0%2C%22text%22%3A%22map%22%2C%22tags%22%3Anull%2C%22categories%22%3A%5B%5D%2C%22gallery_categories%22%3A%5B%5D%2C%22focus_economics_category%22%3A%5B%5D%2C%22parents%22%3A%5B%5D%2C%22sort%22%3A%7B%22last_edit_date%22%3A-1%7D%2C%22search_by%22%3A%22content%22%2C%22search_content_attributes%22%3A%5B%22insights_attributes%22%2C%22insights_content%22%2C%22series_attributes%22%5D%7D&_=1517220044209"
    var req3 = "/api/search/landing/count?q=%7B%22count%22%3A35%2C%22groups%22%3A%5B%22favorite%22%2C%22my%22%2C%22analytics%22%2C%22shared%22%2C%22recent%22%2C%22all%22%5D%2C%22custom_query%22%3A%7B%22favorite%22%3A%7B%22services_codes%22%3A%5B%5D%2C%22products_codes%22%3A%5B%5D%7D%2C%22my%22%3A%7B%22services_codes%22%3A%5B%5D%2C%22products_codes%22%3A%5B%5D%7D%2C%22analytics%22%3A%7B%22services_codes%22%3A%5B%5D%2C%22products_codes%22%3A%5B%5D%7D%2C%22shared%22%3A%7B%22services_codes%22%3A%5B%5D%2C%22products_codes%22%3A%5B%5D%7D%2C%22recent%22%3A%7B%22services_codes%22%3A%5B%5D%2C%22products_codes%22%3A%5B%5D%7D%2C%22all%22%3A%7B%22services_codes%22%3A%5B%5D%2C%22products_codes%22%3A%5B%5D%7D%7D%2C%22group%22%3A%22my%22%2C%22sub_group%22%3A%22gallery%22%2C%22from%22%3A0%2C%22text%22%3A%22map%22%2C%22tags%22%3Anull%2C%22categories%22%3A%5B%5D%2C%22gallery_categories%22%3A%5B%5D%2C%22focus_economics_category%22%3A%5B%5D%2C%22parents%22%3A%5B%5D%2C%22sort%22%3A%7B%22last_edit_date%22%3A-1%7D%2C%22search_by%22%3A%22content%22%2C%22search_content_attributes%22%3A%5B%22insights_attributes%22%2C%22insights_content%22%2C%22series_attributes%22%5D%7D&_=1517220044210"
    var req4 = "/api/search/landing/tree?q=%7B%22count%22%3A35%2C%22group%22%3A%22my%22%2C%22sub_group%22%3A%22gallery%22%2C%22from%22%3A0%2C%22text%22%3A%22map%22%2C%22tags%22%3Anull%2C%22categories%22%3A%5B%5D%2C%22gallery_categories%22%3A%5B%5D%2C%22focus_economics_category%22%3A%5B%5D%2C%22parents%22%3A%5B%5D%2C%22sort%22%3A%7B%22last_edit_date%22%3A-1%7D%2C%22search_by%22%3A%22content%22%2C%22search_content_attributes%22%3A%5B%22insights_attributes%22%2C%22insights_content%22%2C%22series_attributes%22%5D%7D&_=1517220044211"
  }

  object time {
    var pause = "60"
    pause toInt

  }






  object addSeries {
    var req = "/api/insight/7ea809f8-80ee-47ab-b2a1-ae90c9e95073/series/data_entity/"
    var body = "addSeries.txt"
  }

  object addSeries2 {
    var req = "/api/insight/09d1704c-7400-40a6-aaaf-549f9f814739/series/data_entity/"
    var body = "addSeries.txt"
  }


  object addSeries3 {
    var req = "/api/insight/bbdf5e88-a4ee-492b-b397-404c86eb9a77/series/data_entity/"
    var body = "addSeries3.txt"
  }

  object downloadSeries {
    var req = "/api/v1/export/series/xlsx"
    var body = "downloadSeries.txt"
  }

}
*/
