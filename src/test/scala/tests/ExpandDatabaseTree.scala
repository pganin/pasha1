package tests

import io.gatling.core.Predef._
import io.gatling.http.Predef._


class ExpandDatabaseTree_1 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("expand database tree by 1 user")

    .exec(http("1 request")
      .get(variables.expandTree.req1)
      .headers(variables.global.cookies))

    .exec(http("2 request")
      .get(variables.expandTree.req2)
      .headers(variables.global.cookies))

    .exec(http("3 request")
      .get(variables.expandTree.req3)
      .headers(variables.global.cookies))

    .exec(http("4 request")
      .get(variables.expandTree.req4)
      .headers(variables.global.cookies))

    .exec(http("5 request")
      .get(variables.expandTree.req5)
      .headers(variables.global.cookies))

  setUp(
    scn.inject(
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}







class ExpandDatabaseTree_2 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("expand database tree by 5 users")

    .exec(http("1 request")
      .get(variables.expandTree.req1)
      .headers(variables.global.cookies))

    .exec(http("2 request")
      .get(variables.expandTree.req2)
      .headers(variables.global.cookies))

    .exec(http("3 request")
      .get(variables.expandTree.req3)
      .headers(variables.global.cookies))

    .exec(http("4 request")
      .get(variables.expandTree.req4)
      .headers(variables.global.cookies))

    .exec(http("5 request")
      .get(variables.expandTree.req5)
      .headers(variables.global.cookies))

  setUp(
    scn.inject(
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}







class ExpandDatabaseTree_3 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("expand database tree by 10 users")

    .exec(http("1 request")
      .get(variables.expandTree.req1)
      .headers(variables.global.cookies))

    .exec(http("2 request")
      .get(variables.expandTree.req2)
      .headers(variables.global.cookies))

    .exec(http("3 request")
      .get(variables.expandTree.req3)
      .headers(variables.global.cookies))

    .exec(http("4 request")
      .get(variables.expandTree.req4)
      .headers(variables.global.cookies))

    .exec(http("5 request")
      .get(variables.expandTree.req5)
      .headers(variables.global.cookies))

  setUp(
    scn.inject(
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}



