package tests

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class SearchOnLanding_1 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("search on landing")

    .exec(http("search on landing by 1 user request_1")
      .get(variables.searchLanding.req1)
      .headers(variables.global.cookies))

    .exec(http("search on landing by 1 user request_2")
      .get(variables.searchLanding.req2)
      .headers(variables.global.cookies))

    .exec(http("search on landing by 1 user request_3")
      .get(variables.searchLanding.req3)
      .headers(variables.global.cookies))

    .exec(http("search on landing by 1 user request_4")
      .get(variables.searchLanding.req4)
      .headers(variables.global.cookies))


  setUp(
    scn.inject(
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}







class SearchOnLanding_2 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("search on landing by 5 users")

    .exec(http("search on landing by 5 users request_1")
      .get(variables.searchLanding.req1)
      .headers(variables.global.cookies))

    .exec(http("search on landing by 5 users request_2")
      .get(variables.searchLanding.req2)
      .headers(variables.global.cookies))

    .exec(http("search on landing by 5 users request_3")
      .get(variables.searchLanding.req3)
      .headers(variables.global.cookies))

    .exec(http("search on landing by 5 users request_4")
      .get(variables.searchLanding.req4)
      .headers(variables.global.cookies))


  setUp(
    scn.inject(
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}







class SearchOnLanding_3 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("search on landing by 10 users")

    .exec(http("search on landing by 10 users request_1")
      .get(variables.searchLanding.req1)
      .headers(variables.global.cookies))

    .exec(http("search on landing by 10 users request_2")
      .get(variables.searchLanding.req2)
      .headers(variables.global.cookies))

    .exec(http("search on landing by 10 users request_3")
      .get(variables.searchLanding.req3)
      .headers(variables.global.cookies))

    .exec(http("search on landing by 10 users request_4")
      .get(variables.searchLanding.req4)
      .headers(variables.global.cookies))


  setUp(
    scn.inject(
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}



