package tests

import io.gatling.core.Predef._
import io.gatling.http.Predef._


class CopyGalleryInsight_1 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("copy gallery insight by 1 user")

    .exec(http("copy gallery insight by 1 user")
      .get(variables.copyGalleryInsight.req)
      .headers(variables.global.cookies))


  setUp(
    scn.inject(
      atOnceUsers(1),
      nothingFor(60),
      atOnceUsers(1),
      nothingFor(60),
      atOnceUsers(1),
      nothingFor(60),
      atOnceUsers(1),
      nothingFor(60),
      atOnceUsers(1),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}







class CopyGalleryInsight_2 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("copy gallery insight by 5 users")

    .exec(http("copy gallery insight by 5 users")
      .get(variables.copyGalleryInsight.req)
      .headers(variables.global.cookies))


  setUp(
    scn.inject(
      atOnceUsers(5),
      nothingFor(60),
      atOnceUsers(5),
      nothingFor(60),
      atOnceUsers(5),
      nothingFor(60),
      atOnceUsers(5),
      nothingFor(60),
      atOnceUsers(5),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}








class CopyGalleryInsight_3 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("copy gallery insight by 10 users")

    .exec(http("copy gallery insight by 10 users")
      .get(variables.copyGalleryInsight.req)
      .headers(variables.global.cookies))


  setUp(
    scn.inject(
      atOnceUsers(10),
      nothingFor(60),
      atOnceUsers(10),
      nothingFor(60),
      atOnceUsers(10),
      nothingFor(60),
      atOnceUsers(10),
      nothingFor(60),
      atOnceUsers(10),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}


