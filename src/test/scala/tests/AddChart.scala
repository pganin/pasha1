package tests

import io.gatling.core.Predef._
import io.gatling.http.Predef._


class AddChart_1 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("add chart by 1 user")

    .exec(http("add chart by 1 user")
      .post(variables.addChart.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.addChart.body)))


  setUp(
    scn.inject(
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}







class AddChart_2 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("add chart by 5 users")

    .exec(http("add chart by 5 users")
      .post(variables.addChart.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.addChart.body)))


  setUp(
    scn.inject(
      atOnceUsers(5),
      nothingFor(25),
      atOnceUsers(5),
      nothingFor(25),
      atOnceUsers(5),
      nothingFor(25),
      atOnceUsers(5),
      nothingFor(25),
      atOnceUsers(5),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}






class AddChart_3 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("add chart by 10 users")

    .exec(http("add chart by 10 users")
      .post(variables.addChart.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.addChart.body)))


  setUp(
    scn.inject(
      atOnceUsers(10),
      nothingFor(35),
      atOnceUsers(10),
      nothingFor(35),
      atOnceUsers(10),
      nothingFor(35),
      atOnceUsers(10),
      nothingFor(35),
      atOnceUsers(10),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}


