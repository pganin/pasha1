package tests

import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._




/*
  class StressTests extends Simulation {

    var variables = new variables

    val httpProtocol = http
      .baseURL(variables.global.baseURL)

    val scn1 = scenario("create insight")

      .exec(http("create insight")
        .post(variables.createInsight.req)
        .headers(variables.global.cookies)
        .body(RawFileBody(variables.createInsight.body)))


    val scn2 = scenario("add chart")

      .exec(http("add chart")
        .post(variables.addChart1.req)
        .headers(variables.global.cookies)
        .body(RawFileBody(variables.addChart1.body)))


    val scn3 = scenario("add map")

      .exec(http("add map")
        .post(variables.addMap.req)
        .headers(variables.global.cookies)
        .body(RawFileBody(variables.addMap.body)))


    val scn4 = scenario("add table")

      .exec(http("add table")
        .post(variables.addTable1.req)
        .headers(variables.global.cookies)
        .body(RawFileBody(variables.addTable1.body)))


    val scn5 = scenario("apply filter")

      .exec(http("apply filter")
        .post(variables.applyFilter.req)
        .headers(variables.global.cookies)
        .body(RawFileBody(variables.applyFilter.body)))


    val scn6 = scenario("open releases tab")

      .exec(http("open releases tab")
        .get(variables.openReleasesTab.req)
        .headers(variables.global.cookies))


    val scn7 = scenario("copy gallery insight")

      .exec(http("copy gallery insight")
        .get(variables.copyGalleryInsight.req)
        .headers(variables.global.cookies))


    val scn8 = scenario("download 100 series")

      .exec(http("download 100 series")
        .post(variables.download100Series.req)
        .headers(variables.global.cookies)
        .body(RawFileBody(variables.download100Series.body)))


   /* val scn9 = scenario("expand database tree")

      .exec(http("expand database tree_1 request")
        .get(variables.expandTree.req1)
        .headers(variables.global.cookies))

      .exec(http("expand database tree_2 request")
        .get(variables.expandTree.req2)
        .headers(variables.global.cookies))

      .exec(http("expand database tree_3 request")
        .get(variables.expandTree.req3)
        .headers(variables.global.cookies))

      .exec(http("expand database tree_4 request")
        .get(variables.expandTree.req4)
        .headers(variables.global.cookies))

      .exec(http("expand database tree_5 request")
        .get(variables.expandTree.req5)
        .headers(variables.global.cookies))*/


    val scn10 = scenario("open all tab")

      .exec(http("open all tab")
        .get(variables.openAllTab.req)
        .headers(variables.global.cookies))


    val scn11 = scenario("search for a keyword")

      .exec(http("search for a keyword")
        .post(variables.searchKeyword.req)
        .headers(variables.global.cookies)
        .body(RawFileBody(variables.searchKeyword.body)))


   /* val scn12 = scenario("search on landing")

      .exec(http("search on landing request_1")
        .get(variables.searchLanding.req1)
        .headers(variables.global.cookies))

      .exec(http("search on landing request_2")
        .get(variables.searchLanding.req2)
        .headers(variables.global.cookies))

      .exec(http("search on landing request_3")
        .get(variables.searchLanding.req3)
        .headers(variables.global.cookies))

      .exec(http("search on landing request_4")
        .get(variables.searchLanding.req4)
        .headers(variables.global.cookies))*/


    val scn13 = scenario("sorting on landing page")

      .exec(http("sorting on landing page")
        .get(variables.sortingLanding.req)
        .headers(variables.global.cookies))


    val scn14 = scenario("open Datasets tab")

      .exec(http("open Datasets tab")
        .get(variables.openDatasets.req)
        .headers(variables.global.cookies))


    setUp(
      scn1.inject(constantUsersPerSec(1) during(32400)).protocols(httpProtocol),
      scn2.inject(constantUsersPerSec(1) during(32400)).protocols(httpProtocol),
      scn3.inject(constantUsersPerSec(1) during(32400)).protocols(httpProtocol),
      scn4.inject(constantUsersPerSec(1) during(32400)).protocols(httpProtocol),
      scn5.inject(constantUsersPerSec(1) during(32400)).protocols(httpProtocol),
      scn6.inject(constantUsersPerSec(1) during(32400)).protocols(httpProtocol),
      scn7.inject(constantUsersPerSec(1) during(32400)).protocols(httpProtocol),
      scn8.inject(constantUsersPerSec(1) during(32400)).protocols(httpProtocol),
    //  scn9.inject(constantUsersPerSec(1) during(10)).protocols(httpProtocol),
      scn10.inject(constantUsersPerSec(1) during(32400)).protocols(httpProtocol),
      scn11.inject(constantUsersPerSec(1) during(32400)).protocols(httpProtocol),
   //   scn12.inject(constantUsersPerSec(1) during(10)).protocols(httpProtocol),
      scn13.inject(constantUsersPerSec(1) during(32400)).protocols(httpProtocol),
      scn14.inject(constantUsersPerSec(1) during(32400)).protocols(httpProtocol)
    )
  }


class StressTests_1 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("create insight by 1 user")

    .exec(http("create insight by 1 user")
      .post(variables.createInsight.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.createInsight.body)))

  setUp(
    scn.inject(
      atOnceUsers(1)
    ).protocols(httpProtocol)
  )
}
*/
