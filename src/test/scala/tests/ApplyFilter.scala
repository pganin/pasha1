package tests

import io.gatling.core.Predef._
import io.gatling.http.Predef._


class ApplyFilter_1 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("apply filter by 1 user")

    .exec(http("apply filter by 1 user")
      .post(variables.applyFilter.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.applyFilter.body)))

  setUp(
    scn.inject(
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}






class ApplyFilter_2 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("apply filter by 5 users")

    .exec(http("apply filter by 5 users")
      .post(variables.applyFilter.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.applyFilter.body)))

  setUp(
    scn.inject(
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}






class ApplyFilter_3 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("apply filter by 10 users")

    .exec(http("apply filter by 10 users")
      .post(variables.applyFilter.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.applyFilter.body)))

  setUp(
    scn.inject(
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}

