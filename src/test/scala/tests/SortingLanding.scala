package tests

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class SortingOnLanding_1 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("sorting on landing page by 1 user")

    .exec(http("sorting on landing page by 1 user")
      .get(variables.sortingLanding.req)
      .headers(variables.global.cookies))

  setUp(
    scn.inject(
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}






class SortingOnLanding_2 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("sorting on landing page by 5 users")

    .exec(http("sorting on landing page by 5 users")
      .get(variables.sortingLanding.req)
      .headers(variables.global.cookies))

  setUp(
    scn.inject(
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}







class SortingOnLanding_3 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("sorting on landing page by 10 users")

    .exec(http("sorting on landing page by 10 users")
      .get(variables.sortingLanding.req)
      .headers(variables.global.cookies))

  setUp(
    scn.inject(
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}

