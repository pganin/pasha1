package tests

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class SearchKeyword_1 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("search for a keyword by 1 user")

    .exec(http("search for a keyword by 1 user")
      .post(variables.searchKeyword.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.searchKeyword.body)))

  setUp(
    scn.inject(
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}







class SearchKeyword_2 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("search for a keyword by 5 users")

    .exec(http("search for a keyword by 5 users")
      .post(variables.searchKeyword.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.searchKeyword.body)))

  setUp(
    scn.inject(
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}







class SearchKeyword_3 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("search for a keyword by 10 users")

    .exec(http("search for a keyword by 10 users")
      .post(variables.searchKeyword.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.searchKeyword.body)))

  setUp(
    scn.inject(
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}


