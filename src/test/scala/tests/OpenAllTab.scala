package tests

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class OpenAllTab_1 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("open all tab by 1 user")

    .exec(http("open all tab by 1 user")
      .get(variables.openAllTab.req)
      .headers(variables.global.cookies))


  setUp(
    scn.inject(
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}







class OpenAllTab_2 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("open all tab by 5 users")

    .exec(http("open all tab by 5 users")
      .get(variables.openAllTab.req)
      .headers(variables.global.cookies))


  setUp(
    scn.inject(
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}








class OpenAllTab_3 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("open all tab by 10 users")

    .exec(http("open all tab by 10 users")
      .get(variables.openAllTab.req)
      .headers(variables.global.cookies))


  setUp(
    scn.inject(
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}


