package tests

import io.gatling.core.Predef._
import io.gatling.http.Predef._


class AddMap_1 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("add map by 1 user")

    .exec(http("add map by 1 user")
      .post(variables.addMap.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.addMap.body)))


  setUp(
    scn.inject(
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}






class AddMap_2 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("add map by 5 users")

    .exec(http("add map by 5 users")
      .post(variables.addMap.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.addMap.body)))


  setUp(
    scn.inject(
      atOnceUsers(5),
      nothingFor(25),
      atOnceUsers(5),
      nothingFor(25),
      atOnceUsers(5),
      nothingFor(25),
      atOnceUsers(5),
      nothingFor(25),
      atOnceUsers(5),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}






class AddMap_3 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("add map by 10 users")

    .exec(http("add map by 10 users")
      .post(variables.addMap.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.addMap.body)))


  setUp(
    scn.inject(
      atOnceUsers(10),
      nothingFor(35),
      atOnceUsers(10),
      nothingFor(35),
      atOnceUsers(10),
      nothingFor(35),
      atOnceUsers(10),
      nothingFor(35),
      atOnceUsers(10),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}

