package tests

import io.gatling.core.Predef._
import io.gatling.http.Predef._


class AddTable_1 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("add table by 1 user")

    .exec(http("add table by 1 user")
      .post(variables.addTable.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.addTable.body)))


  setUp(
    scn.inject(
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}






class AddTable_2 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("add table by 5 users")

    .exec(http("add table by 5 users")
      .post(variables.addTable.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.addTable.body)))


  setUp(
    scn.inject(
      atOnceUsers(5),
      nothingFor(25),
      atOnceUsers(5),
      nothingFor(25),
      atOnceUsers(5),
      nothingFor(25),
      atOnceUsers(5),
      nothingFor(25),
      atOnceUsers(5),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}







class AddTable_3 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("add table by 10 users")

    .exec(http("add table by 10 users")
      .post(variables.addTable.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.addTable.body)))


  setUp(
    scn.inject(
      atOnceUsers(10),
      nothingFor(35),
      atOnceUsers(10),
      nothingFor(35),
      atOnceUsers(10),
      nothingFor(35),
      atOnceUsers(10),
      nothingFor(35),
      atOnceUsers(10),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}



