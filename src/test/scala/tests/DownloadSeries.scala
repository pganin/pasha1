package tests

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class DownloadSeries_1 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("download 100 series by 1 user")

    .exec(http("download 100 series by 1 user")
      .post(variables.download100Series.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.download100Series.body)))


  setUp(
    scn.inject(
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}







class DownloadSeries_2 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("download 100 series by 5 users")

    .exec(http("download 100 series by 5 users")
      .post(variables.download100Series.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.download100Series.body)))


  setUp(
    scn.inject(
      atOnceUsers(5),
      nothingFor(25),
      atOnceUsers(5),
      nothingFor(25),
      atOnceUsers(5),
      nothingFor(25),
      atOnceUsers(5),
      nothingFor(25),
      atOnceUsers(5),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}







class DownloadSeries_3 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("download 100 series by 10 users")

    .exec(http("download 100 series by 10 users")
      .post(variables.download100Series.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.download100Series.body)))


  setUp(
    scn.inject(
      atOnceUsers(10),
      nothingFor(35),
      atOnceUsers(10),
      nothingFor(35),
      atOnceUsers(10),
      nothingFor(35),
      atOnceUsers(10),
      nothingFor(35),
      atOnceUsers(10),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}



