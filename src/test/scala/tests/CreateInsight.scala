package tests

import io.gatling.core.Predef._
import io.gatling.http.Predef._





class CreateInsight_1 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("create insight by 1 user")

    .exec(http("create insight by 1 user")
      .post(variables.createInsight.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.createInsight.body)))

  setUp(
    scn.inject(
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(15),
      atOnceUsers(1),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}







class CreateInsight_2 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("create insight by 5 users")

    .exec(http("create insight by 5 users")
      .post(variables.createInsight.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.createInsight.body)))

  setUp(
    scn.inject(
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(15),
      atOnceUsers(5),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}







class CreateInsight_3 extends Simulation {

  var variables = new variables

  val httpProtocol = http
    .baseURL(variables.global.baseURL)

  val scn = scenario("create insight by 10 users")

    .exec(http("create insight by 10 users")
      .post(variables.createInsight.req)
      .headers(variables.global.cookies)
      .body(RawFileBody(variables.createInsight.body)))

  setUp(
    scn.inject(
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(15),
      atOnceUsers(10),
      nothingFor(180)
    ).protocols(httpProtocol)
  )
}


