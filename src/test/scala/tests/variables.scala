package tests

/**
  * Created by User on 18.11.2017.
  */
class variables {


  object global {
    val cookies = Map(
      "cookie" -> "mssid=s%3Ae6f96e16ea73243ae492bd4bce872d8.9T%2BhesSJ44FAEZ8LSqI14EM6r8DuWdyq9k2tAEFENag",
      "content-type" -> "application/json"
    )
    val baseURL = "https://stage.ceicdata.com"
  }

  object createInsight {
    var req = "/api/insight?ct=1517219161792"
    var body = "createInsight.txt"
  }

  object copyInsight {
    var req = "/api/insight/6c9a516e-f0a7-443a-85e8-dc58b24bb39f/copy?name=123_copy&_=1511182232255"
  }

  object applyFilter {
    var req = "/api/series"
    var body = "applyFilter.txt"
  }

  object openInsight {
    var req = "/api/insight/a83a7b0e-cc23-4f2e-9cc9-184008d856e9/series?_=1511524535924"
  }

  object expandTree {
    var req2 = "/api/nodes/databases/GLOBAL%26%26ALL/topics?filter=%7B%22database_series_filter%22%3A%22%22%2C%22databases%22%3A%5B%5D%2C%22status%22%3A%5B%5D%2C%22name_only%22%3Afalse%2C%22subscribed_only%22%3Atrue%2C%22database_series_sort_type%22%3A%7B%22dir%22%3A%22asc%22%2C%22field%22%3A%22TREE%22%7D%2C%22observation_filter%22%3A%7B%22first_obs_before_selected%22%3Afalse%2C%22last_obs_after_selected%22%3Afalse%2C%22first_obs_before%22%3Anull%2C%22last_obs_after%22%3Anull%7D%2C%22unit%22%3A%5B%5D%2C%22source%22%3A%5B%5D%2C%22region%22%3A%5B%5D%2C%22countries%22%3A%5B%5D%2C%22frequencies%22%3A%5B%5D%2C%22indicator%22%3A%5B%5D%2C%22tree_matches_mode%22%3A%22MISMATCHES_ONLY%22%2C%22new_series_only%22%3Afalse%2C%22key_series_only%22%3Afalse%2C%22forecast_only%22%3Afalse%2C%22with_release_only%22%3Afalse%2C%22with_revision_only%22%3Afalse%2C%22with_replacements_only%22%3Afalse%2C%22load_full_tree%22%3Atrue%7D&search_tab=tree&_=1527157051706"
    var req3 = "/api/nodes/topics/TP43530/sections?filter=%7B%22database_series_filter%22%3A%22%22%2C%22databases%22%3A%5B%5D%2C%22status%22%3A%5B%5D%2C%22name_only%22%3Afalse%2C%22subscribed_only%22%3Atrue%2C%22database_series_sort_type%22%3A%7B%22dir%22%3A%22asc%22%2C%22field%22%3A%22TREE%22%7D%2C%22observation_filter%22%3A%7B%22first_obs_before_selected%22%3Afalse%2C%22last_obs_after_selected%22%3Afalse%2C%22first_obs_before%22%3Anull%2C%22last_obs_after%22%3Anull%7D%2C%22unit%22%3A%5B%5D%2C%22source%22%3A%5B%5D%2C%22region%22%3A%5B%5D%2C%22countries%22%3A%5B%5D%2C%22frequencies%22%3A%5B%5D%2C%22indicator%22%3A%5B%5D%2C%22tree_matches_mode%22%3A%22MISMATCHES_ONLY%22%2C%22new_series_only%22%3Afalse%2C%22key_series_only%22%3Afalse%2C%22forecast_only%22%3Afalse%2C%22with_release_only%22%3Afalse%2C%22with_revision_only%22%3Afalse%2C%22with_replacements_only%22%3Afalse%2C%22load_full_tree%22%3Atrue%7D&search_tab=tree&_=1527157051707"
    var req4 = "/api/nodes/sections/SC50093/tables?filter=%7B%22database_series_filter%22%3A%22%22%2C%22databases%22%3A%5B%5D%2C%22status%22%3A%5B%5D%2C%22name_only%22%3Afalse%2C%22subscribed_only%22%3Atrue%2C%22database_series_sort_type%22%3A%7B%22dir%22%3A%22asc%22%2C%22field%22%3A%22TREE%22%7D%2C%22observation_filter%22%3A%7B%22first_obs_before_selected%22%3Afalse%2C%22last_obs_after_selected%22%3Afalse%2C%22first_obs_before%22%3Anull%2C%22last_obs_after%22%3Anull%7D%2C%22unit%22%3A%5B%5D%2C%22source%22%3A%5B%5D%2C%22region%22%3A%5B%5D%2C%22countries%22%3A%5B%5D%2C%22frequencies%22%3A%5B%5D%2C%22indicator%22%3A%5B%5D%2C%22tree_matches_mode%22%3A%22MISMATCHES_ONLY%22%2C%22new_series_only%22%3Afalse%2C%22key_series_only%22%3Afalse%2C%22forecast_only%22%3Afalse%2C%22with_release_only%22%3Afalse%2C%22with_revision_only%22%3Afalse%2C%22with_replacements_only%22%3Afalse%2C%22load_full_tree%22%3Atrue%7D&search_tab=tree&_=1527157051708"
    var req5 = "/api/nodes/tables/TB184843/series?filter=%7B%22database_series_filter%22%3A%22%22%2C%22databases%22%3A%5B%5D%2C%22status%22%3A%5B%5D%2C%22name_only%22%3Afalse%2C%22subscribed_only%22%3Atrue%2C%22database_series_sort_type%22%3A%7B%22dir%22%3A%22asc%22%2C%22field%22%3A%22TREE%22%7D%2C%22observation_filter%22%3A%7B%22first_obs_before_selected%22%3Afalse%2C%22last_obs_after_selected%22%3Afalse%2C%22first_obs_before%22%3Anull%2C%22last_obs_after%22%3Anull%7D%2C%22unit%22%3A%5B%5D%2C%22source%22%3A%5B%5D%2C%22region%22%3A%5B%5D%2C%22countries%22%3A%5B%5D%2C%22frequencies%22%3A%5B%5D%2C%22indicator%22%3A%5B%5D%2C%22tree_matches_mode%22%3A%22MISMATCHES_ONLY%22%2C%22new_series_only%22%3Afalse%2C%22key_series_only%22%3Afalse%2C%22forecast_only%22%3Afalse%2C%22with_release_only%22%3Afalse%2C%22with_revision_only%22%3Afalse%2C%22with_replacements_only%22%3Afalse%2C%22load_full_tree%22%3Atrue%7D&search_tab=tree&_=1527157051709"
    var req1 = "/api/nodes/databases/GLOBAL/categories?filter=%7B%22database_series_filter%22%3A%22%22%2C%22databases%22%3A%5B%5D%2C%22status%22%3A%5B%5D%2C%22name_only%22%3Afalse%2C%22subscribed_only%22%3Atrue%2C%22database_series_sort_type%22%3A%7B%22dir%22%3A%22asc%22%2C%22field%22%3A%22TREE%22%7D%2C%22observation_filter%22%3A%7B%22first_obs_before_selected%22%3Afalse%2C%22last_obs_after_selected%22%3Afalse%2C%22first_obs_before%22%3Anull%2C%22last_obs_after%22%3Anull%7D%2C%22unit%22%3A%5B%5D%2C%22source%22%3A%5B%5D%2C%22region%22%3A%5B%5D%2C%22countries%22%3A%5B%5D%2C%22frequencies%22%3A%5B%5D%2C%22indicator%22%3A%5B%5D%2C%22tree_matches_mode%22%3A%22MISMATCHES_ONLY%22%2C%22new_series_only%22%3Afalse%2C%22key_series_only%22%3Afalse%2C%22forecast_only%22%3Afalse%2C%22with_release_only%22%3Afalse%2C%22with_revision_only%22%3Afalse%2C%22with_replacements_only%22%3Afalse%2C%22load_full_tree%22%3Atrue%7D&search_tab=tree&_=1527157051705"
  }

  object searchKeyword {
    var req = "/api/series"
    var body = "searchKeyword.txt"
  }

  object openDatasets {
    var req = "/api/data_sets?parameters=%7B%22main_filter%22%3A%7B%22tree_matches_mode%22%3A%22MISMATCHES_ONLY%22%2C%22indicator%22%3A%5B%5D%2C%22frequencies%22%3A%5B%5D%2C%22countries%22%3A%5B%5D%2C%22region%22%3A%5B%5D%2C%22source%22%3A%5B%5D%2C%22unit%22%3A%5B%5D%2C%22new_series_only%22%3Afalse%2C%22key_series_only%22%3Afalse%2C%22observation_filter%22%3A%7B%22last_obs_after%22%3Anull%2C%22first_obs_before%22%3Anull%2C%22last_obs_after_selected%22%3Afalse%2C%22first_obs_before_selected%22%3Afalse%7D%2C%22database_series_sort_type%22%3A%7B%22field%22%3A%22TREE%22%2C%22dir%22%3A%22asc%22%7D%2C%22subscribed_only%22%3Afalse%2C%22name_only%22%3Afalse%2C%22status%22%3A%5B%5D%2C%22databases%22%3A%5B%5D%2C%22database_series_filter%22%3A%22%22%7D%2C%22can_use_cache%22%3Atrue%2C%22from%22%3A0%2C%22to%22%3A50%7D&search_tab=DATASETS&_=1517220044195"
  }

  object copyGalleryInsight {
    var req = "/api/insight/2c6b9264-1453-4284-b416-4436c6ad2060/copy?name=Russia%20-%20Macro%20Overview_copy&_=1526635899108"
  }

  object openAllTab {
    var req = "/api/data_sets?parameters=%7B%22additional_filter%22%3A%7B%22sort_by%22%3A%22NEW%22%2C%22sort_order%22%3A%22desc%22%7D%2C%22can_use_cache%22%3Atrue%2C%22from%22%3A0%2C%22to%22%3A10%7D&search_tab=ALL&_=1517220044193"
  }

  object openReleasesTab {
    var req = "/api/release_schedule/indicators?parameters=%7B%22main_filter%22%3A%7B%22tree_matches_mode%22%3A%22MISMATCHES_ONLY%22%2C%22indicator%22%3A%5B%5D%2C%22frequencies%22%3A%5B%5D%2C%22countries%22%3A%5B%5D%2C%22region%22%3A%5B%5D%2C%22source%22%3A%5B%5D%2C%22unit%22%3A%5B%5D%2C%22new_series_only%22%3Afalse%2C%22key_series_only%22%3Afalse%2C%22observation_filter%22%3A%7B%22last_obs_after%22%3Anull%2C%22first_obs_before%22%3Anull%2C%22last_obs_after_selected%22%3Afalse%2C%22first_obs_before_selected%22%3Afalse%7D%2C%22database_series_sort_type%22%3A%7B%22field%22%3A%22TREE%22%2C%22dir%22%3A%22asc%22%7D%2C%22subscribed_only%22%3Afalse%2C%22name_only%22%3Afalse%2C%22status%22%3A%5B%5D%2C%22databases%22%3A%5B%5D%2C%22database_series_filter%22%3A%22%22%7D%2C%22additional_filter%22%3A%7B%22status%22%3A%5B%5D%2C%22from%22%3A%222018-01-28T21%3A00%3A00.000Z%22%2C%22to%22%3A%222018-01-29T20%3A59%3A59.999Z%22%7D%2C%22from%22%3A0%2C%22to%22%3A50%7D&search_tab=release_schedule&_=1517220044194"
  }

  object download100Series {
    var req = "/api/v1/export/series/xlsx"
    var body = "download100Series.txt"
  }

  object addChart1 {
    var req = "/api/track/Chart/b436b042-379e-4a51-96e6-50f426119831"
    var body = "addChart1.txt"
  }

  object addChart {
    var req = "/api/insight/f3147b67-1b2e-495f-bc70-e10824f0cb9f/views/3e65a805-b65d-4d27-a9f2-abd2826c63d4/elements?ct=1526640208374"
    var body = "addChart.txt"
  }

  object addTable {
    var req = "/api/insight/c6c3a820-b5b1-41e4-9c38-e6e86d8634cd/views/1e2b72fc-83b0-4c33-b9f3-3a4ab5d48b04/elements?ct=1526640311470"
    var body = "addTable.txt"
  }

  object addTable1 {
    var req = "/api/track/Table/c5f1ebcb-f7e0-4e2a-91d2-eeb87ab33c5c"
    var body = "addTable1.txt"
  }

  object addMap {
   var req = "/api/insight/138ab2c3-029f-4890-9636-73458224c2e9/views/366d6d46-6166-4f64-88f8-f205ba165779/elements?ct=1526639608755"
    var body = "addMap.txt"
  }

  object addMap1 {
    var req = "/api/track/Map/138ab2c3-029f-4890-9636-73458224c2e9"
    var body = "addMap1.txt"
  }

  object sortingLanding {
    var req = "/api/search/landing/insights?q=%7B%22count%22%3A35%2C%22group%22%3A%22all%22%2C%22sub_group%22%3A%22gallery%22%2C%22from%22%3A0%2C%22text%22%3A%22%22%2C%22tags%22%3Anull%2C%22categories%22%3A%5B%5D%2C%22gallery_categories%22%3A%5B%5D%2C%22focus_economics_category%22%3A%5B%5D%2C%22parents%22%3A%5B%5D%2C%22sort%22%3A%7B%22name%22%3A-1%7D%2C%22search_by%22%3A%22content%22%2C%22search_content_attributes%22%3A%5B%22insights_attributes%22%2C%22insights_content%22%2C%22series_attributes%22%5D%7D&_=1517220044221"
  }

  object searchLanding {
    var req1 = "/api/search/landing/insights?q=%7B%22count%22%3A35%2C%22group%22%3A%22my%22%2C%22sub_group%22%3A%22gallery%22%2C%22from%22%3A0%2C%22text%22%3A%22map%22%2C%22tags%22%3Anull%2C%22categories%22%3A%5B%5D%2C%22gallery_categories%22%3A%5B%5D%2C%22focus_economics_category%22%3A%5B%5D%2C%22parents%22%3A%5B%5D%2C%22sort%22%3A%7B%22last_edit_date%22%3A-1%7D%2C%22search_by%22%3A%22content%22%2C%22search_content_attributes%22%3A%5B%22insights_attributes%22%2C%22insights_content%22%2C%22series_attributes%22%5D%7D&_=1517220044208"
    var req2 = "/api/search/landing/series?q=%7B%22count%22%3A35%2C%22group%22%3A%22my%22%2C%22sub_group%22%3A%22gallery%22%2C%22from%22%3A0%2C%22text%22%3A%22map%22%2C%22tags%22%3Anull%2C%22categories%22%3A%5B%5D%2C%22gallery_categories%22%3A%5B%5D%2C%22focus_economics_category%22%3A%5B%5D%2C%22parents%22%3A%5B%5D%2C%22sort%22%3A%7B%22last_edit_date%22%3A-1%7D%2C%22search_by%22%3A%22content%22%2C%22search_content_attributes%22%3A%5B%22insights_attributes%22%2C%22insights_content%22%2C%22series_attributes%22%5D%7D&_=1517220044209"
    var req3 = "/api/search/landing/count?q=%7B%22count%22%3A35%2C%22groups%22%3A%5B%22favorite%22%2C%22my%22%2C%22analytics%22%2C%22shared%22%2C%22recent%22%2C%22all%22%5D%2C%22custom_query%22%3A%7B%22favorite%22%3A%7B%22services_codes%22%3A%5B%5D%2C%22products_codes%22%3A%5B%5D%7D%2C%22my%22%3A%7B%22services_codes%22%3A%5B%5D%2C%22products_codes%22%3A%5B%5D%7D%2C%22analytics%22%3A%7B%22services_codes%22%3A%5B%5D%2C%22products_codes%22%3A%5B%5D%7D%2C%22shared%22%3A%7B%22services_codes%22%3A%5B%5D%2C%22products_codes%22%3A%5B%5D%7D%2C%22recent%22%3A%7B%22services_codes%22%3A%5B%5D%2C%22products_codes%22%3A%5B%5D%7D%2C%22all%22%3A%7B%22services_codes%22%3A%5B%5D%2C%22products_codes%22%3A%5B%5D%7D%7D%2C%22group%22%3A%22my%22%2C%22sub_group%22%3A%22gallery%22%2C%22from%22%3A0%2C%22text%22%3A%22map%22%2C%22tags%22%3Anull%2C%22categories%22%3A%5B%5D%2C%22gallery_categories%22%3A%5B%5D%2C%22focus_economics_category%22%3A%5B%5D%2C%22parents%22%3A%5B%5D%2C%22sort%22%3A%7B%22last_edit_date%22%3A-1%7D%2C%22search_by%22%3A%22content%22%2C%22search_content_attributes%22%3A%5B%22insights_attributes%22%2C%22insights_content%22%2C%22series_attributes%22%5D%7D&_=1517220044210"
    var req4 = "/api/search/landing/tree?q=%7B%22count%22%3A35%2C%22group%22%3A%22my%22%2C%22sub_group%22%3A%22gallery%22%2C%22from%22%3A0%2C%22text%22%3A%22map%22%2C%22tags%22%3Anull%2C%22categories%22%3A%5B%5D%2C%22gallery_categories%22%3A%5B%5D%2C%22focus_economics_category%22%3A%5B%5D%2C%22parents%22%3A%5B%5D%2C%22sort%22%3A%7B%22last_edit_date%22%3A-1%7D%2C%22search_by%22%3A%22content%22%2C%22search_content_attributes%22%3A%5B%22insights_attributes%22%2C%22insights_content%22%2C%22series_attributes%22%5D%7D&_=1517220044211"
  }

  object time {
    var pause = "60"
    pause toInt

  }



}
